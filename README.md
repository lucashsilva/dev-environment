# HOW TO SETUP AND RUN DEV ENVIRONMENT

## Motivation

Because our environment is based on several services, it might be a bit tricky to manually set an environment to easily work and debug. In order to solve this problem, this project gathers all services in a docker compose file, which has all services linked to each other and consumes only necessary resources. That means you neither have to open an IDE window for each project, nor run each service on command line manually. Also, you only have to change the configurations necessary to achieve your goal.

## Before you start

Make sure you have Java, Maven, Quarkus and Docker installed in your machine. Check the [wiki](https://wiki.is.altb.co/doc/configuring-development-environment-k7HgKZi7CV/) for more details. 

## Accessing Gitlab Maven Repository

This project references extensions kept in the Gitlab Maven private package manager. To access this repository, a Gitlab access token must be configured in the Maven configuration files.
This Maven configuration file can be located in different places, but by default it's located in `${maven.home}/conf/settings.xml` or `${user.home}/.m2/settings.xml`.
The token must be generated in your Gitlab profile, and it must have `read_registry` permission. Here is the example that should be followed:

```
<settings>
    <servers>
        <server>
            <id>gitlab-maven</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Token</name>
                        <value>YOUR_TOKEN_HERE</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```
Alternatively, you can define the variable `GITLAB_READ_PACKAGE_REGISTRY_TOKEN` in your profile startup script, and change the Maven default settings to look to the settings.xml in this project running the following commands:

`echo "export GITLAB_READ_PACKAGE_REGISTRY_TOKEN=YOUR_TOKEN_HERE" >> ~/.zshrc`

`source ~/.zshrc`

`./mvnw package -Pnative -s settings.xml`

## Logging into docker repository

Run `docker login registry.gitlab.com`. Enter your Gitlab username and password.

## Running the services 🚀

In order to run all services:
1. Clone this repository and open the directory.
2. Run `docker compose up`. This command will pull all the images that are necessary to run the services and then boot them up.

The services **with the latest images from our repositories** should be running after the steps above. By default, these images are not debuggable - you can only run them.

## Coding and debugging

Now you know how to boot the services, let's learn how to develop and debug anything the fastest way. Quarkus offers a remote dev mode, which is being used by this local environment.

In order to be able to debug/make changes to any of the services locally:

1. Copy the file `.env.example` to `.env`.
2. Modify the file `.env`, replacing the local paths for the service repo you want to work on and for `QUARKUS_EXTENSIONS_PATH`, which is mandatory for all services. It can be a relative path. You don't have to include the ones you don't want to debug or make changes to.
3. Run `./prepare.sh` from your terminal. This will build your local projects and install what's necessary for the next steps (skipping tests).
4. Open `docker-compose.yml`, find your service and comment the line that starts with `image:`, and then uncomment the whole line block that starts with `build`. This tells Docker to build from your local code instead of the Gitlab image. 
5. Run `docker compose up --build`.

You can open any of the services on your favorite IDE, start coding and have your changes reflected with live reload as follows:

### Getting the latest images from Gitlab

If you are using the images from the Gitlab registry and want to have the latest version available, run `docker compose build --pull`.

### Live reload (Quarkus only)

If you want to see your changes reflected while you code, run the following command in the directory of the service you want to work on:

`
mvn quarkus:remote-dev -Ddebug=false \
  -Dquarkus.package.type=mutable-jar \
  -Dquarkus.live-reload.url=http://localhost:LIVE_RELOAD_PORT \
  -Dquarkus.live-reload.password=123
`

Replace **PORT** with your service port, from the detailed service mapping table. After a short build, Quarkus will be listening for any changes you make in the code. Make your changes and try a new request, and you'll see that Quarkus will reload whatever has been changed.

You can also create a custom run configuration within your ide passing the maven goal from the command above.

### Debugging

If you want to debug the service, create a new run configuration of type "Remote JVM Debug" within your IDE pointing to the **DEBUG_PORT** assigned to the service and run it. This works for both Quarkus and Spring Boot.

It should look like this (IntelliJ IDEA):

![Run configuration](./assets/docker-debug.png)

### Live reload AND debugging (Quarkus)

You can combine the two approaches above to be able to have live reload and debug at the same time, just run the live reload command and run your debugger afterwards :) 

If you don't want to have to run the live reload command every time, you can also add a "Before Launch" task of type "Maven Goal" to the debugger run configuration with the Maven goal that you would call in the command line. 

See screenshot below:

![Maven goal](./assets/maven-goal.png)

In "Command line", enter this:

`quarkus:remote-dev -Ddebug=false -Dquarkus.package.type=mutable-jar -Dquarkus.live-reload.url=http://localhost:PORT -Dquarkus.live-reload.password=123` 


## Detailed service mapping

| Service        | Http/Live Reload Port | Debug Port | Application Type    | Healthcheck Path      |
|----------------|-----------------------|------------|---------------------|-----------------------|
| Members        | 8180                  | 5105       | Quarkus             | /q/health             |
| Datafilter     | 8280                  | 5205       | Quarkus             | /q/health             |
| Configurations | 8380                  | 5305       | Quarkus             | /q/health             |
| Onboarding     | 8480 / no live reload | 5405       | Spring Boot/Camunda | /healthcheck/liveness |

#### Have fun!


