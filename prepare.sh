if [ -f .env ]; then
  export $(echo $(cat .env | sed 's/#.*//g'| xargs) | envsubst)
fi

CURRENT_DIR=$PWD;

if [ ! -z "$QUARKUS_EXTENSIONS_PATH" ]; then
  cd $(echo $QUARKUS_EXTENSIONS_PATH | tr -d '\r')

  mvn install -DskipTests

  cd $(echo $CURRENT_DIR | tr -d '\r')
fi

if [ ! -z "$MEMBERS_SERVICE_PATH" ]; then

  cd $(echo $MEMBERS_SERVICE_PATH | tr -d '\r')

  ./mvnw package -Dquarkus.package.type=mutable-jar -DskipTests

  cd $(echo $CURRENT_DIR | tr -d '\r')
fi

if [ ! -z "$DATAFILTER_SERVICE_PATH" ]; then
  cd $(echo $DATAFILTER_SERVICE_PATH | tr -d '\r')

  ./mvnw package -Dquarkus.package.type=mutable-jar -DskipTests

  cd $(echo $CURRENT_DIR | tr -d '\r')
fi

if [ ! -z "$CONFIGURATIONS_SERVICE_PATH" ]; then
  cd $(echo $CONFIGURATIONS_SERVICE_PATH | tr -d '\r')

  ./mvnw package -Dquarkus.package.type=mutable-jar -DskipTests

  cd $(echo $CURRENT_DIR | tr -d '\r')
fi

if [ ! -z "$CONFIGURATIONS_SERVICE_PATH" ]; then
  cd $(echo $ONBOARDING_SERVICE_PATH | tr -d '\r')

  mvn package -DskipTests
fi

echo "Done!"